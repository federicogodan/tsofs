#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <linux/fs.h>
#include <sys/stat.h>
#include <linux/types.h>
#include "../tsofs.h"

int main(int argc, char **argv)
{
	fprintf(stderr, "<tsofs>: Creating filesystem...\n");
	struct tsofs_dentry dir;
	struct tsofs_superblock sb;
	struct tsofs_inode inode;
	time_t tm;
	off_t nsectors = TSOFS_MAXBLOCKS;
	int devfd, error, i;
	char block[TSOFS_BLOCK_SIZE];

	if (argc != 2) {
		fprintf(stderr, "<tsofs>: Need to specify device\n");
		exit(1);
	}
	devfd = open(argv[1], O_WRONLY);
	error = lseek(devfd, (off_t) (nsectors * 512), SEEK_SET);
	if (error == -1) {
		fprintf(stderr, "<tsofs>: Cannot create filesystem of specified size\n");
		exit(1);
	}

	lseek(devfd, 0, SEEK_SET);

	memset((void *)&block, 0, TSOFS_BLOCK_SIZE);
	for (i = 0; i < TSOFS_MAXBLOCKS; i++) {
		write(devfd, block, TSOFS_BLOCK_SIZE);
	}

	lseek(devfd, 0, SEEK_SET);

	sb.tsb_magic = TSOFS_MAGIC_NUMBER;
	// sb.tsb_mod = 0;
	sb.tsb_nifree = TSOFS_MAXFILES - 1; // inodo 0 reservado y inodo 1 para root
	sb.tsb_nbfree = TSOFS_MAXBLOCKS - 1;
	sb.tsb_inode[1] = TSOFS_INODE_INUSE; // asigno inodo de root en uso
	for (i = 2; i < TSOFS_MAXFILES; i++) 
		sb.tsb_inode[i] = TSOFS_INODE_FREE; // el resto libres
	
	sb.tsb_block[0] = TSOFS_BLOCK_INUSE; // asigno block con dentrys de root en uso
	for (i = 0; i < TSOFS_MAXBLOCKS; i++)
		sb.tsb_block[i] = TSOFS_BLOCK_FREE; // el resto libres

	write(devfd, (char *)&sb, sizeof(struct tsofs_superblock));
	
	/* inicializacion de root */
	time(&tm);
	memset((void *)&inode, 0, sizeof(struct tsofs_inode));
	inode.ti_ino = 1;
	inode.ti_mode = S_IFDIR | 0755;
	inode.ti_nlink = 2;
	inode.ti_atime = inode.ti_mtime = inode.ti_ctime = tm;
	inode.ti_uid = 0;
	inode.ti_gid = 0;
	inode.ti_size = TSOFS_BLOCK_SIZE;
	inode.ti_cant_dentry = 2;
	inode.ti_block = TSOFS_FIRST_DATA_BLOCK;
	lseek(devfd, (TSOFS_FIRST_INODE_BLOCK + 1) * TSOFS_BLOCK_SIZE, SEEK_SET);
	write(devfd, (char *)&inode, sizeof(struct tsofs_inode));

	// referencias al mismo directorio root
	lseek(devfd, TSOFS_FIRST_DATA_BLOCK * TSOFS_BLOCK_SIZE, SEEK_SET);
	memset((void *)&block, 0, TSOFS_BLOCK_SIZE);
	write(devfd, block, TSOFS_BLOCK_SIZE);
	lseek(devfd, (TSOFS_FIRST_DATA_BLOCK) * TSOFS_BLOCK_SIZE, SEEK_SET);
	dir.td_ino = 1;
	strcpy(dir.td_name, ".");
	write(devfd, (char *)&dir, sizeof(struct tsofs_dentry));
	dir.td_ino = 1;
	strcpy(dir.td_name, "..");
	write(devfd, (char *)&dir, sizeof(struct tsofs_dentry));

	fprintf(stderr, "<tsofs>: Filesystem created!\n");
	return 0;


}
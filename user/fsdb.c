/*--------------------------------------------------------------*/
/*---------------------------- fsdb.c --------------------------*/
/*--------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <linux/fs.h>
#include <linux/types.h>
#include "../tsofs.h"

struct tsofs_superblock sb;
int devfd;

void print_inode(int inum, struct tsofs_inode *uip)
{
	char buf[TSOFS_BLOCK_SIZE];
	struct tsofs_dentry *dentry;
	time_t time;
	int x;

	printf("\ninode number %d\n", inum);
	printf("  ti_mode     = %x\n", uip->ti_mode);
	printf("  ti_nlink    = %d\n", uip->ti_nlink);
	time = uip->ti_atime;
	printf("  ti_atime    = %s", ctime(&time));
	time = uip->ti_mtime;
	printf("  ti_mtime    = %s", ctime(&time));
	time = uip->ti_ctime;
	printf("  ti_ctime    = %s", ctime(&time));
	printf("  ti_uid      = %d\n", uip->ti_uid);
	printf("  ti_gid      = %d\n", uip->ti_gid);
	printf("  ti_size     = %d\n", uip->ti_size);
	printf("  ti_cant_dentry = %3d ", uip->ti_cant_dentry);
	printf("  ti_block = %3d ", uip->ti_block);

	/*
	 * Print out the directory entries
	 */

	if (uip->ti_mode & S_IFDIR) {
		printf("\n\n  Directory entries:\n");
		lseek(devfd, uip->ti_block * TSOFS_BLOCK_SIZE, SEEK_SET);
		read(devfd, buf, TSOFS_BLOCK_SIZE);
		dentry = (struct tsofs_dentry *)buf;
		for (x = 0; x < TSOFS_DIRS_PER_BLOCK; x++) {
			if (dentry->td_ino != 0) {
				printf("    inum[%2d],"
				       "name[%s]\n",
				       dentry->td_ino,
				       dentry->td_name);
			}
			dentry++;
		}
		printf("\n");
	} else
		printf("\n\n");
}

int read_inode(ino_t inum, struct tsofs_inode *uip)
{
	if (sb.tsb_inode[inum] == TSOFS_INODE_FREE) {
		printf("WARNING: INODE LISTED AS FREE IN SB\n");
	}
	lseek(devfd, (TSOFS_FIRST_INODE_BLOCK * TSOFS_BLOCK_SIZE) + (inum * TSOFS_BLOCK_SIZE), SEEK_SET);
	read(devfd, (char *)uip, sizeof(struct tsofs_inode));

	return 0;
}

int main(int argc, char **argv)
{
	struct tsofs_inode inode;
	char command[512];
	ino_t inum;
	char dataText[512];

	devfd = open(argv[1], O_RDWR);
	if (devfd < 0) {
		fprintf(stderr, "<tsofs> Failed to open device\n");
		exit(1);
	}

	/*
	 * Read in and validate the superblock
	 */

	read(devfd, (char *)&sb, sizeof(struct tsofs_superblock));
	if (sb.tsb_magic != TSOFS_MAGIC_NUMBER) {
		printf("This is not a tsofs filesystem\n");
		exit(1);
	}

	while (1) {
		printf("tsofsdb > ");
		fflush(stdout);
		scanf("%s", command);
		if (command[0] == 'q')
			exit(0);
		if (command[0] == 'i') {
			inum = atoi(&command[1]);
			read_inode(inum, &inode);
			print_inode(inum, &inode);
		}
		if (command[0] == 's') {
			printf("\nSuperblock contents:\n");
			printf("  ts_magic   = 0x%x\n", sb.tsb_magic);
			printf("  ts_nifree  = %d\n", sb.tsb_nifree);
			printf("  ts_nbfree  = %d\n\n", sb.tsb_nbfree);
		}
		if (command[0] == 'd') {
			inum = atoi(&command[1]);
			printf("block number requested: %lu\n", inum);
			lseek(devfd, (inum + TSOFS_FIRST_DATA_BLOCK) * TSOFS_BLOCK_SIZE, SEEK_SET);
			read(devfd, &dataText, TSOFS_BLOCK_SIZE);
			if (!dataText[0])
				printf("Data block empty\n");
			else
				printf("Data: \n %s \n", dataText);
		}
	}
}

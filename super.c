/*
 * Libera los recursos del superbloque
 */
static void tsofs_put_super(struct super_block *sb)
{	
	struct tsofs_fs *tsofs = (struct tsofs_fs *)sb->s_fs_info;
	brelse(tsofs->tu_sbh);
	kfree(sb->s_fs_info);

	printk(KERN_WARNING "<tsofs> tsofs_put_super\n");
}


/* 
 * SUPERBLOCK OPERATIONS 
 */
static struct super_operations tsofs_super_ops = {
	.put_super = tsofs_put_super,
};

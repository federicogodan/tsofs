static struct inode *tsofs_make_inode(struct super_block *sb,  int mode, __u32 ti_ino, __u32 ti_addr);
static struct dentry *tsofs_inode_lookup(struct inode *parent_inode, struct dentry *child_dentry, unsigned int flags);
static int tsofs_create_dir_file(struct super_block *sb, struct dentry *dir, const char *name, int is_dir);
static int tsofs_inode_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode);
static int tsofs_inode_touch(struct inode *dir, struct dentry *dentry, umode_t mode, bool excl) ;

/*
 * INODE OPERATIONS
 */
static struct inode_operations tsofs_inode_ops = {
        .create = tsofs_inode_touch,
        .lookup = tsofs_inode_lookup,
        .mkdir  = tsofs_inode_mkdir,
};

/*
 * Agrega un dentry con nombre @name y referencia a @tso_ime dentro del inodo @tso_inode
 */
int tsofs_write_dentry(struct super_block *sb, struct tsofs_inode *tso_inode, struct tsofs_inode *tso_ime, const char *name) 
{       
        struct tsofs_dentry *tso_dentry;
        int i;
        struct buffer_head *bh;
        struct buffer_head *bh_i;
        struct tsofs_inode *tso_inodetmp;

        printk(KERN_WARNING "<tsofs> tsofs_write_dentry\n");

        bh = sb_bread(sb, tso_inode->ti_block);
        tso_dentry = (struct tsofs_dentry *) bh->b_data;
        for (i = 0; i < tso_inode->ti_cant_dentry; i++) {
                tso_dentry++;
        }

        strcpy(tso_dentry->td_name, name);
        tso_dentry->td_ino = tso_ime->ti_ino;
        tso_inode->ti_cant_dentry++;
        tso_inode->ti_nlink++;
        mark_buffer_dirty(bh);
        sync_dirty_buffer(bh);

        /* PATCH: llevo a disco la actualizacion de ti_cant_dentry y links */
        bh_i = sb_bread(sb, TSOFS_FIRST_INODE_BLOCK + tso_inode->ti_ino);
        tso_inodetmp = (struct tsofs_inode *) bh_i->b_data;
        tso_inodetmp->ti_cant_dentry = tso_inode->ti_cant_dentry;
        tso_inodetmp->ti_nlink = tso_inode->ti_nlink;
        mark_buffer_dirty(bh_i);
        sync_dirty_buffer(bh_i);
        brelse(bh_i);
        brelse(bh);

        return 0;

}

/*
 * Obtengo el inodo con indice @ino dentro del sistema
 */
struct inode *tsofs_get_inode(struct super_block *sb, unsigned long ino)
{
        struct buffer_head *bh;
        struct tsofs_inode *tso_in;
        struct inode *inode;
        int block;

        printk(KERN_WARNING "<tsofs> tsofs_get_inode\n");

        inode = iget_locked(sb, ino);
        if (!inode)
                return ERR_PTR(-ENOMEM);
        if (!(inode->i_state & I_NEW))
                return inode;

        /* if (ino < TSOFS_ROOT_INO || ino > TSOFS_MAXFILES) {
         *         printk(KERN_ERR "<tsofs>: Bad inode number %lu\n", ino);
         *         return -1;
         * }
         */

        block = TSOFS_FIRST_INODE_BLOCK + ino;
        bh = sb_bread(inode->i_sb, block);
        tso_in = (struct tsofs_inode *) (bh->b_data);

        inode->i_mode = tso_in->ti_mode;
        if (tso_in->ti_mode & S_IFDIR) { 
                /* directorios */
                printk(KERN_ERR "<tsofs>: tsofs_get_inode: es directorio\n");
                inode->i_mode |= S_IFDIR;
                inode->i_op = &tsofs_inode_ops;
                inode->i_fop = &tsofs_dir_ops;
        } else if (tso_in->ti_mode & S_IFREG) { 
                /* archivos */
                printk(KERN_ERR "<tsofs>: tsofs_get_inode: es archivo\n");
                inode->i_mode |= S_IFREG;
                inode->i_op = &tsofs_inode_ops;
                inode->i_fop = &tsofs_file_ops;
        }
        set_nlink(inode, tso_in->ti_nlink);
        inode->i_atime.tv_sec = tso_in->ti_atime;
        inode->i_mtime.tv_sec = tso_in->ti_mtime;
        inode->i_ctime.tv_sec = tso_in->ti_ctime;
        inode->i_uid.val = tso_in->ti_uid;
        inode->i_gid.val = tso_in->ti_gid;
        inode->i_size = tso_in->ti_size;
        inode->i_private = tso_in;
        inode->i_sb = sb;
        brelse(bh);
        unlock_new_inode(inode);
        
        return inode;
}

/*
 * Creo un nuevo inodo en el sistema dentro del inidice de @inodos inumber y con bloque de datos @block
 */
static struct inode *tsofs_make_inode(struct super_block *sb,  int mode, __u32 inumber, __u32 block)
{

        struct tsofs_inode *tso_inode;
        struct buffer_head *bh;
        struct buffer_head *bh_dentry;
        struct inode *inode = new_inode(sb);
        struct tsofs_dentry *dentry;

        printk(KERN_WARNING "<tsofs> tsofs_make_inode\n");

        bh = sb_bread(sb, TSOFS_FIRST_INODE_BLOCK + inumber);
        tso_inode = (struct tsofs_inode *) bh->b_data;
        inode->i_private = tso_inode;
        tso_inode->ti_ino = inumber;
        tso_inode->ti_mode = mode;
        tso_inode->ti_atime = tso_inode->ti_mtime = tso_inode->ti_ctime = CURRENT_TIME.tv_sec;
        tso_inode->ti_uid = tso_inode->ti_gid = 0;   
        tso_inode->ti_block = TSOFS_FIRST_DATA_BLOCK + block;

        inode->i_mode = mode;
        inode->i_sb = sb;
        inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;

        if (mode & S_IFDIR) {
                printk(KERN_WARNING "<tsofs> tsofs_make_inode: es un directorio\n");
                tso_inode->ti_cant_dentry = 2; /* para "."" y ".." */
                tso_inode->ti_mode = S_IFDIR | 0755;
                tso_inode->ti_nlink = 2; /* links de "."" y ".." */
                tso_inode->ti_size = TSOFS_BLOCK_SIZE; /* los directios "ocupan" un bloque entero siempre */
                inode->i_op = &tsofs_inode_ops;
                inode->i_fop = &tsofs_dir_ops;

                /* incializar "." y ".." */
                bh_dentry = sb_bread(sb, TSOFS_FIRST_DATA_BLOCK + block);
                dentry = (struct tsofs_dentry *)bh_dentry->b_data;
                dentry->td_ino = inumber;
                strcpy(dentry->td_name, ".");

                dentry++;

                dentry->td_ino = inumber;
                strcpy(dentry->td_name, "..");
                mark_buffer_dirty(bh_dentry);
                sync_dirty_buffer(bh_dentry);
                brelse(bh_dentry);

        } else if (mode & S_IFREG) {
                printk(KERN_WARNING "<tsofs> tsofs_make_inode: es un archivo\n");
                tso_inode->ti_size = 0;
                tso_inode->ti_mode |= S_IFREG;
                inode->i_op = &tsofs_inode_ops;
                inode->i_fop = &tsofs_file_ops;
        }
        
        mark_buffer_dirty(bh);
        sync_dirty_buffer(bh);
        brelse(bh);
        return inode;
}

/* 
 * Crea directorios o archivos en el sistema (segun indique @is_dir) con nombre @name
 */
static int tsofs_create_dir_file(struct super_block *sb, struct dentry *dir, const char *name, int is_dir) 
{
        int i, b = 0;
        struct inode *inode;
        struct tsofs_superblock *tso_sb;
        struct tsofs_inode *tso_inode;
        struct tsofs_fs *tsofs;

        printk(KERN_WARNING "<tsofs> tsofs_create_dir_file\n");
        tsofs = (struct tsofs_fs *) sb->s_fs_info;
        tso_sb = tsofs->tu_sb;

        /* verificar haya inodos y bloques disponibles */
        if (tso_sb->tsb_nbfree <= 0 || tso_sb->tsb_nifree <= 0) {
                printk(KERN_WARNING "<tsofs> tsofs_create_dir_file no hay inodos: %u o bloques: %u\n", tsofs->tu_sb->tsb_nifree, tsofs->tu_sb->tsb_nbfree);
                return -1;
        }
        printk(KERN_WARNING "<tsofs> tsofs_create_dir_file hay inodos: %u o bloques: %u\n", tsofs->tu_sb->tsb_nifree, tsofs->tu_sb->tsb_nbfree);

        /* verifico que directorio padre puede tener un hijo mas */
        tso_inode = (struct tsofs_inode *) dir->d_parent->d_inode->i_private;
        if (tso_inode->ti_cant_dentry >= TSOFS_DIRS_PER_BLOCK) {
                printk(KERN_WARNING "<tsofs> tsofs_create_dir_file el padre esta lleno\n");
                return -1;
        }

        /* obtengo inodo libre */
        i = 1;
        while(i < TSOFS_MAXFILES) {
                if (!tsofs->tu_sb->tsb_inode[i])
                        break;
                i++;
        }
        tso_sb->tsb_inode[i] = TSOFS_INODE_INUSE;
        tso_sb->tsb_nifree--;

        /* obtengo bloque libre */
        b = 1;
        while (b < TSOFS_MAXBLOCKS) {
                if (!tsofs->tu_sb->tsb_block[b])
                        break;
                b++;
        }
        printk(KERN_WARNING "<tsofs> tsofs_create_dir_file: se ocupara inodo: %i y bloque de datos: %i\n", i, b);
        tso_sb->tsb_block[b] = TSOFS_BLOCK_INUSE;
        tso_sb->tsb_nbfree--;

        /* creo el nuevo inodo para el directorio */
        if (is_dir) {
                /* directorio */
                inode = tsofs_make_inode(sb, S_IFDIR | 0644, i, b);

                /* agregar dentry al padre */
                tsofs_write_dentry(sb, (struct tsofs_inode *) dir->d_parent->d_inode->i_private, (struct tsofs_inode *) inode->i_private, name); // agrega un entry de nombre name al padre
        } else {
                /* archivo */
                inode = tsofs_make_inode(sb, S_IFREG | 0644, i, b);
                tsofs_write_dentry(sb, (struct tsofs_inode *) dir->d_parent->d_inode->i_private, (struct tsofs_inode *) inode->i_private, name);        
        }

        /* sincronizo con el superbloque en disco */
        mark_buffer_dirty(tsofs->tu_sbh);
        sync_dirty_buffer(tsofs->tu_sbh);

        return 0;
}

/*
 * Busqueda de elementos dentro del directorio por nombre
 */
static struct dentry *tsofs_inode_lookup(struct inode *parent_inode, struct dentry *child_dentry, unsigned int flags)
{
        struct super_block *sb = parent_inode->i_sb;
        struct tsofs_inode *parent = (struct tsofs_inode *) parent_inode->i_private;
        struct tsofs_inode *tso_itmp; 
        struct buffer_head *bh;
        struct tsofs_dentry *tso_de;
        struct inode *inode;
        int i;

        printk(KERN_WARNING "<tsofs> tsofs_lookup\n");

        bh = sb_bread(sb, parent->ti_block);
        printk("<tsofs> tsofs_lookup: in: ino=%u, block=%u\n", parent->ti_ino, parent->ti_block);

        /* recorro los direntrys del directorio en busca de archivo */
        tso_de = (struct tsofs_dentry *)bh->b_data;
        for (i = 0; i < parent->ti_cant_dentry; i++) {
                printk("<tsofs> tsofs_lookup: tiene archivo '%s' (ino=%u)\n", tso_de->td_name, tso_de->td_ino);

                if (!strcmp(tso_de->td_name, child_dentry->d_name.name)) {
                        /* encontrado */
                        inode = tsofs_get_inode(sb, tso_de->td_ino);
                        tso_itmp = (struct tsofs_inode *) inode->i_private;
                        inode_init_owner(inode, parent_inode, tso_itmp->ti_mode);
                        d_add(child_dentry, inode);
                        return 0;
                }
                tso_de++;
        }

        printk(KERN_ERR "<tsofs> tsofs_lookup: no se encontro inodo para el nomre [%s]\n", child_dentry->d_name.name);
        return 0;
}

/*
 * Creacion de archivos
 */
int tsofs_inode_touch(struct inode *dir, struct dentry *dentry, umode_t mode, bool excl) 
{
        int is_dir = 0; /* 0: archivo */

        printk(KERN_WARNING "<tsofs> tsofs_inode_touch\n");

        return tsofs_create_dir_file(dir->i_sb, dentry, dentry->d_name.name, is_dir);
}

/*
 * Creacion de directorios
 */
int tsofs_inode_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode) 
{
        int is_dir = 1; /* 1: directorio */

        printk(KERN_WARNING "<tsofs> tsofs_inode_mkdir\n");

        return tsofs_create_dir_file(dir->i_sb, dentry, dentry->d_name.name, is_dir);
}
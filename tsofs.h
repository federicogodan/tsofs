#define TSOFS_DIRS_PER_BLOCK	16	/* cantidad de dentrys por directorio: TSOFS_BLOCK_SIZE/sizeof(tsofs_dentry) */
#define TSOFS_DIR_NAMELEN	28	/* largo maximo de nombres de directorios/archivos */

#define TSOFS_BLOCK_SIZE 	512	/* tamano de bloque */
#define TSOFS_MAXFILES   	32 	/* cantidad maxima de inodos */
#define TSOFS_MAXBLOCKS  	460	/* cantidad maxima de bloques de datos */

#define TSOFS_FIRST_DATA_BLOCK	50	/* indice de primer bloque de datos */
#define TSOFS_FIRST_INODE_BLOCK	8	/* indice de primer bloque de inodos */
#define TSOFS_ROOT_INO		1	/* indice del inodo root */

#define TSOFS_MAGIC_NUMBER 0x66666666	/* magic number del filesystem */


/* AUXILIARES */
#define TSOFS_INODE_FREE	0
#define TSOFS_INODE_INUSE	1

#define TSOFS_BLOCK_FREE	0
#define TSOFS_BLOCK_INUSE	1

#define TSOFS_FSCLEAN		0
#define TSOFS_FSDIRTY		1

/*
 * El superbloque esta en el bloque 0 del fs
 * Los siguientes bloques contienen los inodos del sistema (TSOFS_MAXFILES)
 * Luego se encuentran los bloques de datos asociados a cada inodo
 */

struct tsofs_superblock {
	__u32 tsb_magic; 			/* magic number */
	__u32 tsb_nifree; 			/* cantidad de inodos libres */
	__u32 tsb_nbfree; 			/* cantidad de bloques libres */
	char  tsb_inode[TSOFS_MAXFILES];	/* bitmap inodos libres/ocupados */
	char  tsb_block[TSOFS_MAXBLOCKS]; 	/* bitmap bloques libres/ocupados */
};

struct tsofs_inode {
	__u32 ti_ino; 		/* numero de inodo: indice dentro de los bloques de inodos*/
	__u32 ti_mode; 		/* tipo de inodo archivo o directorio */
	__u32 ti_nlink; 	/* cantidad de hardlinks asociados */
	__u32 ti_atime; 	/* fecha de acceso*/
	__u32 ti_mtime; 	/* fecha de modificacion */
	__u32 ti_ctime; 	/* fecha de creacion */
	__u32 ti_uid;		/* usuario */
	__u32 ti_gid;		/* grupo */
	__u32 ti_size; 		/* tamano del directorio/archivo */
	__u32 ti_cant_dentry;	/* cantidad de tsofs_dentry: utilizado solo por directorios */
	__u32 ti_block;		/* indice de bloque de datos con info guardada */
};

struct tsofs_dentry {
	__u32 td_ino;				/* num de inodo asociado */
	char td_name[TSOFS_DIR_NAMELEN]; 	/* nombre del archivo */
};

struct tsofs_fs {
	struct tsofs_superblock *tu_sb;	/* referencia al superbloque */
	struct buffer_head *tu_sbh;	/* referencia al buffer_head del superbloque*/
};
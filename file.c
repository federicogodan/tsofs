/*
 * Lectura de archivos
 */
ssize_t tsofs_file_read(struct file * fil, char * buf, size_t size, loff_t* lof) 
{

	struct tsofs_inode *inode = (struct tsofs_inode *) fil->f_path.dentry->d_inode->i_private;
	struct buffer_head *bh;
	char *buffer;
	int nbytes;
	int res;

	printk(KERN_WARNING "<tsofs> tsofs_file_read\n");

	if (*lof >= inode->ti_size) {
		return 0;
	}

	bh = sb_bread(fil->f_path.dentry->d_inode->i_sb, inode->ti_block);
	buffer = (char *)bh->b_data;
	nbytes = min((size_t)inode->ti_size, size);
	res = copy_to_user(buf, buffer, nbytes);
	brelse(bh);

	*lof += nbytes;
	return nbytes;
}

/*
 * Escritura de archivos
 */
ssize_t tsofs_file_write (struct file * fil, const char * buf, size_t size, loff_t * lof) 
{
	
	struct tsofs_inode *inode = (struct tsofs_inode *) fil->f_path.dentry->d_inode->i_private;
	struct buffer_head *bh;
	char *buffer;
	struct tsofs_inode *inode_bh;
	int res;

	printk(KERN_WARNING "<tsofs> tsofs_file_write\n");

	bh = sb_bread(fil->f_path.dentry->d_inode->i_sb, inode->ti_block);
	buffer = (char *)bh->b_data;
	buffer += *lof;
	res = copy_from_user(buffer, buf, size);
	*lof += size;
	mark_buffer_dirty(bh);
	sync_dirty_buffer(bh);
	brelse(bh);

	/* actualizo tamano del archivo */
	inode->ti_size = *lof;
	fil->f_path.dentry->d_inode->i_size = *lof;
	bh = sb_bread(fil->f_path.dentry->d_inode->i_sb, TSOFS_FIRST_INODE_BLOCK + inode->ti_ino);
	inode_bh = (struct tsofs_inode *) bh;
	inode_bh->ti_size = inode->ti_size;
	mark_buffer_dirty(bh);
	sync_dirty_buffer(bh);
	brelse(bh);

	return size;
}

/*
 * FILE OPERATIONS
 */
static struct file_operations tsofs_file_ops = {
	.owner = THIS_MODULE,
	.read  = tsofs_file_read,
	.write = tsofs_file_write,
};



/*
 *  Listado de elementos en directorios
 */
int tsofs_dir_iterate_shared(struct file *filp, struct dir_context *ctx) 
{
	loff_t pos;
	struct inode *inode;
	struct super_block *sb;
	struct buffer_head *bh;
	struct tsofs_inode *tso_i;
	struct tsofs_dentry *tso_de;
	int i;

	printk(KERN_WARNING "<tsofs> tsofs_dir_iterate_shared\n");

	pos = ctx->pos;
	inode = file_inode(filp);
	sb = inode->i_sb;

	/* PATCH: evita loop infinto al listar archivos */
	if (pos) {
		return 0;
	}

	tso_i = (struct tsofs_inode *) inode->i_private;
	if (unlikely(!S_ISDIR(tso_i->ti_mode))) {
		printk(KERN_ERR "<tsofs> tsofs_dir_iterate_shared: inode not a directory\n");
		return -ENOTDIR;
	}

	bh = sb_bread(sb, tso_i->ti_block);
	tso_de = (struct tsofs_dentry *)bh->b_data;
	for (i = 0; i < tso_i->ti_cant_dentry; i++) {
		printk(KERN_ERR "<tsofs> tsofs_dir_iterate_shared: leo entry %s i:%d\n", tso_de->td_name, i);
		dir_emit(ctx, tso_de->td_name, TSOFS_DIR_NAMELEN, tso_de->td_ino, DT_UNKNOWN);
		ctx->pos += sizeof(struct tsofs_dentry);
		pos += sizeof(struct tsofs_dentry);
		tso_de++;
	}

	brelse(bh);
	return 0;
}


/*
 * DIRECTORY OPERATIONS
 */
static struct file_operations tsofs_dir_ops = {
	.owner          = THIS_MODULE, 
	.iterate_shared = tsofs_dir_iterate_shared,
};

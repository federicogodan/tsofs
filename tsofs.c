#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
// #include <linux/pagemap.h>      /* PAGE_CACHE_SIZE */
#include <linux/buffer_head.h>
#include <linux/slab.h>

#include <asm/uaccess.h>

#include "tsofs.h"
#include "super.c"
#include "file.c"
#include "inode.c"

/*
 * Inicializa el superbloque con los datos almacenados en disco
 */
static int tsofs_fill_sb(struct super_block *sb, void *data, int silent)
{
        struct inode *root;
        struct tsofs_superblock *tsb; 
        struct buffer_head *bh;
        struct tsofs_fs *tsofs;

        printk(KERN_WARNING "<tsofs> tsofs_fill_sb\n");

        sb_set_blocksize(sb, TSOFS_BLOCK_SIZE);
        bh = sb_bread(sb, 0);
        tsb = (struct tsofs_superblock *) bh->b_data;
        tsofs = kmalloc(sizeof(struct tsofs_fs), GFP_KERNEL);
        tsofs->tu_sb = tsb;
        tsofs->tu_sbh = bh;

        sb->s_blocksize = TSOFS_BLOCK_SIZE;
        sb->s_magic = tsb->tsb_magic;
        sb->s_op = &tsofs_super_ops;
        root = tsofs_get_inode(sb, 1);  /* obtengo el inodo root del fs */
        sb->s_root = d_make_root(root); /* asigno como root el inodo */
        sb->s_fs_info = tsofs;          /* guardo nuestras estructuras para proximos accesos */

        return 0;
}

/*
 * Montado del filesystem
 */
static struct dentry *tsofs_mount(struct file_system_type *fs_type, int flags, const char *dev_name, void *data)
{
        struct dentry *entry;

        printk(KERN_WARNING "<tsofs> tsofs_mount\n");
        entry = mount_bdev(fs_type, flags, dev_name, data, tsofs_fill_sb); /* delegamos a tso_fill_sb cargar el filesystem */

        return entry;
}

/* 
 * FILESYSTEM DEFINITION 
 */
static struct file_system_type tsofs_type = {
        .owner          = THIS_MODULE,
        .name           = "tsofs",
        .mount          = tsofs_mount,
        .kill_sb        = kill_block_super, /* dejamos el por defecto */
        .fs_flags       = FS_REQUIRES_DEV,
};

/*
 * Inicializacion y finalizacion del modulo
 */
static int __init tsofs_init(void)
{
        printk(KERN_WARNING "<tsofs> tsofs_init: module loaded\n");
        return register_filesystem(&tsofs_type);
}

static void __exit tsofs_fini(void)
{
        printk(KERN_WARNING "<tsofs> tsofs_fini: module unloaded\n");
        unregister_filesystem(&tsofs_type);
}

module_init(tsofs_init);
module_exit(tsofs_fini);

/* Metadatos del modulo */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mauro Picó");
MODULE_AUTHOR("Federico Godán");